# Special Thanks

Thank you to the following users for their contributions to cloud-lock:
- [scherzoso](https://github.com/scherzoso) - Fixing multiple monitors and basically rewriting everything
- [kcarbon](https://github.com/kcarbon) - make image configurable, inspiration to add date and time, testing
- [Font Awesome by Dave Gandy](http://fortawesome.github.com/Font-Awesome) - Lock icons
