# `cloud-lock`
`cloud-lock` is a fork of [`i3lock-next`](https://github.com/owenthewizard/i3lock-next). Changes include:
* name change for scripts: `i3lock-next` -> `cloud-lock`
* make image a configurable option
* use Python script instead of bash
* aesthetics
  * replaces the default lock images with an image from FFVII
  * updates parameters passed to i3lock-color (eg. clock, colors, etc.)

# i3lock-next

`i3lock-next` is a Python 3 script and C helper program much like [i3lock-fancy](https://github.com/meskarune/i3lock-fancy). i3lock-next aims to be much faster by using [Imlib2](https://docs.enlightenment.org/api/imlib2/html/index.html) rather than ImageMagick, and being written (mostly) in C.

![screenshot: main](media/screenshot-main.png)

[video: usage](media/video-usage.mkv)

## Features

- Built-in support for custom lock images: the `i3lock` indicator will scale automatically.
- Speed: `i3lock-next` is very fast because most of the image manipulation is done in C.
- Multi-monitor support: what it says on the tin.

## Dependencies

- [i3lock-color](https://github.com/chrjguill/i3lock-color) - a fork of i3lock that supports custom ring colors
- [Imlib2](https://docs.enlightenment.org/api/imlib2/html/)
- [bash](https://www.gnu.org/software/bash/)
- [fontconfig](https://www.freedesktop.org/wiki/Software/fontconfig/)
- [libXrandr](https://www.x.org/wiki/libraries/libxrandr/)

Most of these should be available via the package manager for your distribution.

## Installation

```
$ make
# make install
```

To use a custom prefix for the installation directory:

```
$ make PREFIX=/your/custom/prefix
# make install PREFIX=/your/custom/prefix
```

## Usage

```
cloud-lock [-h|--help] [prompt] [font] [size]

Options:
	-h, --help  Display this help text.

	prompt      Prompt string to display, default is none.

	font        Font to to use, default is Sans.

	size        Size of font, default is 18.
```

### Examples

**Custom Font and Prompt:**

```
$ cloud-lock "Input password" "DejaVu Sans Mono" 24
```

![screenshot: custom font and prompt](media/screenshot-custom.png)

**Default Prompt (None):**

```
$ cloud-lock
```

![screenshot: default font](media/screenshot-default.png)

## Attributions

- Lock Icons: Font Awesome by Dave Gandy - http://fortawesome.github.com/Font-Awesome
- See [THANKS.md](THANKS.md) for more.
